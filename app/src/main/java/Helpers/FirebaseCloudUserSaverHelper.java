package Helpers;

import android.content.Context;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.securityapp.MainActivity;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;

public class FirebaseCloudUserSaverHelper {
    public void saveUser(Context context) {
        String TAG = MainActivity.class.getSimpleName();
        String iid = InstanceID.getInstance(context).getId();
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        FirebaseApp.initializeApp(context);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(
                (@NonNull Task<InstanceIdResult> task) -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    // Отправляем push-token в Firestore
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    Map<String, Object> token_map = new HashMap<>();
                    token_map.put("token", token);
                    token_map.put("timestamp", com.google.firebase.Timestamp.now());
                    token_map.put("android id", android_id);
                    db.collection("tokens").document(iid)
                            .set(token_map)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "DocumentSnapshot successfully written!");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error writing document", e);
                                }
                            });
                    // Log
                    Log.d(TAG, "New token: " + token);
                });
    }
}
