package Helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkhttpImageDownloader extends AsyncTask<String, Void, Bitmap> {
    OkHttpClient client = new OkHttpClient();

    @Override
    protected Bitmap doInBackground(String... params) {

        Request.Builder builder = new Request.Builder();
        if (params[0] == null) {
            return null;
        }
        builder.url(params[0]);
        Request request = builder.build();

        try {
            Response response = client.newCall(request).execute();
            return BitmapFactory.decodeStream(response.body().byteStream());
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
