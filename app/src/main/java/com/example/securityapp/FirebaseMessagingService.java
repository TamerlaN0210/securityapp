package com.example.securityapp;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;


import Helpers.FirebaseCloudUserSaverHelper;
import Helpers.OkhttpImageDownloader;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    String TAG = FirebaseMessagingService.class.getSimpleName();
    final String channelID = "itis.ID";
    final String channelName = "ITIS CHANNEL";
    String url, title, description;
    Bitmap image;

    public FirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() == 0) {
            Log.e(TAG, "Empty data-push");
            return;
        }

        if (remoteMessage.getData().size() > 0) {
            Intent changeStateIntent = new Intent("alert");
            sendBroadcast(changeStateIntent);
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            // загружаем картинку
            url = remoteMessage.getData().get(Conf.dpImageUrl);
            title = remoteMessage.getData().get(Conf.dpTitle);
            description = remoteMessage.getData().get(Conf.dpDescription);

            OkhttpImageDownloader okHttpHandler = new OkhttpImageDownloader();
            try {
                image = okHttpHandler.execute(url).get();
            } catch (Exception e) {
                Log.d(TAG, "Something wrong ", e);
            }

            // создаем действие во время тапа по уведомлению
            Intent resultIntent = new Intent(this, MainActivity.class);
            // добавляем url картинки для отображения в активити
            resultIntent.putExtra(Conf.dpImageUrl, url);
            resultIntent.putExtra(Conf.dpTitle, title);
            resultIntent.putExtra(Conf.dpDescription, description);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            // создаем нотификацию
            NotificationChannel itisChannel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_HIGH);
            Uri alarmSound = Uri.parse("android.resource://com.example.securityapp/sounds/alarm_alarm_alarm");
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelID)
                    .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                    .setContentTitle(remoteMessage.getData().get(Conf.dpTitle))
                    .setContentText(remoteMessage.getData().get(Conf.dpDescription))
                    .setLargeIcon(image)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(image))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(resultPendingIntent)
                    .setSound(alarmSound);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(itisChannel);
            mNotificationManager.notify(1, mBuilder.build());

            // запоминаем сработку системы
            MainActivity.isAlarm = true;
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        new FirebaseCloudUserSaverHelper().saveUser(getApplicationContext());
    }
}
