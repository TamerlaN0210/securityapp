package com.example.securityapp;

/**
 * Вспомогательный класс для хранения переменных конфигурации
 */
public class Conf {
    // URL для запросов к API
    static String urlGetStatus = "http://192.168.14.229:5000/security/status";
    static String urlSetOn = "http://192.168.14.229:5000/security/on";
    static String urlSetOff = "http://192.168.14.229:5000/security/off";

    // названия полей в дата-пуш уведомлении
    static String dpImageUrl = "image_url";
    static String dpTitle = "title";
    static String dpDescription = "desc";
}
