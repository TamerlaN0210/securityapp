package com.example.securityapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StatusFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StatusFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StatusFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {
    String TAG = MainActivity.class.getSimpleName();
    FragmentActivity currentActivity = getActivity();
    final OkHttpClient client = new OkHttpClient();
    ToggleButton btnSwitchSystemState;
    TextView tvSystemStatus;
    ProgressBar progressBar;
    ImageView shieldImage;
    BroadcastReceiver br;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public StatusFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StatusFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StatusFragment newInstance(String param1, String param2) {
        StatusFragment fragment = new StatusFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        currentActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_status, container, false);
        shieldImage = (ImageView) view.findViewById(R.id.imageView2);
        btnSwitchSystemState = (ToggleButton) view.findViewById(R.id.btnSwitchSystemState);
        btnSwitchSystemState.setOnCheckedChangeListener(this);
        tvSystemStatus = (TextView) view.findViewById(R.id.textViewSystemStatus);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        // смотрим, сработала ли сигнализация
        if(MainActivity.isAlarm != null && MainActivity.isAlarm == true) {
            shieldImage.setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
        }

        // запрашиваем состояние
        String statusUrl = Conf.urlGetStatus;
        Request request = new Request.Builder()
                .url(statusUrl)
                .build();
        progressBar.setVisibility(View.VISIBLE);
        shieldImage.setVisibility(View.INVISIBLE);
        tvSystemStatus.setText(R.string.system_update);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO обработка ошибки запроса статуса
                currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(currentActivity.getApplicationContext(), "Не удалось узнать состояние системы", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.INVISIBLE);
                        shieldImage.setVisibility(View.VISIBLE);
                        tvSystemStatus.setText(R.string.system_unknown);
                        shieldImage.setColorFilter(getResources().getColor(R.color.colorYellow), PorterDuff.Mode.SRC_ATOP);
                    }
                });
                Log.d(TAG, "Реквест не выполнился");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, "Реквест выполнился");
                String responseString = response.body().string();
                Log.d("RequestJson", responseString);
                try {
                    JsonParser jsonParser = new JsonParser();
                    JsonElement jsonTree = jsonParser.parse(responseString);
                    JsonObject jsonObject = jsonTree.getAsJsonObject();
                    JsonElement jsonElement = jsonObject.get("status");
                    String status = jsonElement.getAsString();

                    Log.d("Status:", status);
                    if (!jsonObject.has("status")) {
                        Log.d(TAG, "В JSON нет поля status");
                        return;
                    }
                    if (status.equals("1")) {
                        currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvSystemStatus.setText(R.string.system_active);
                                btnSwitchSystemState.setOnCheckedChangeListener(null);
                                btnSwitchSystemState.setChecked(true);
                                btnSwitchSystemState.setOnCheckedChangeListener(StatusFragment.this::onCheckedChanged);
                                shieldImage.setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_ATOP);
                                MainActivity.isAlarm = false;
                            }
                        });
                    }
                    if (status.equals("0")) {
                        currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvSystemStatus.setText(R.string.system_inactive);
                                btnSwitchSystemState.setOnCheckedChangeListener(null);
                                btnSwitchSystemState.setChecked(false);
                                btnSwitchSystemState.setOnCheckedChangeListener(StatusFragment.this::onCheckedChanged);
                                shieldImage.setColorFilter(getResources().getColor(R.color.colorGrey), PorterDuff.Mode.SRC_ATOP);
                                MainActivity.isAlarm = false;
                            }
                        });
                    }
                    if (status.equals("2")) {
                        currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvSystemStatus.setText(R.string.system_alarm);
                                btnSwitchSystemState.setOnCheckedChangeListener(null);
                                btnSwitchSystemState.setChecked(false);
                                btnSwitchSystemState.setOnCheckedChangeListener(StatusFragment.this::onCheckedChanged);
                                MainActivity.isAlarm = true;
                                shieldImage.setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                            }
                        });
                    }

                } catch (Exception e) {
                    Log.d(TAG, "Ошибка при обработке JSON", e);
                }
                currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                        shieldImage.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "Получили сообщение в канале");
                MainActivity.isAlarm = true;
                shieldImage.setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tvSystemStatus.setText(R.string.system_alarm);
            }
        };
        // создаем фильтр для BroadcastReceiver
        IntentFilter intFilt = new IntentFilter("alert");
        // регистрируем (включаем) BroadcastReceiver
        currentActivity.registerReceiver(br, intFilt);
        // конец блока запроса состояния
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        // адрес запроса состояния службы
        String setOnUrl = Conf.urlSetOn;
        String setOffUrl = Conf.urlSetOff;
        // Включаем службу
        progressBar.setVisibility(ProgressBar.VISIBLE);
        shieldImage.setVisibility(View.INVISIBLE);
        tvSystemStatus.setText(R.string.system_update);
        Log.d(TAG, "Показываем ProgressBar");
        if (isChecked) {
            Request request = new Request.Builder()
                    .url(setOnUrl)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    // если включить не удалось, возвращаем кнопку в состояние "выкл"
                    currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buttonView.setOnCheckedChangeListener(null);
                            buttonView.setChecked(false);
                            buttonView.setOnCheckedChangeListener(StatusFragment.this::onCheckedChanged);
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            shieldImage.setVisibility(View.VISIBLE);
                            tvSystemStatus.setText(R.string.system_unknown);
                            shieldImage.setColorFilter(getResources().getColor(R.color.colorYellow), PorterDuff.Mode.SRC_ATOP);
                            Toast.makeText(currentActivity.getApplicationContext(), "Включение не удалось", Toast.LENGTH_SHORT).show();
                        }
                    });

                    Log.d(TAG, "Перестаем показываеть ProgressBar");
                    Log.d(TAG, "Включение службы не удалось");
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try (ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful())
                            throw new IOException("Unexpected code " + response);
                        // При успешном запросе меняем состояние поля на "вкл"
                        currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvSystemStatus.setText(R.string.system_active);
                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                                shieldImage.setVisibility(View.VISIBLE);
                                shieldImage.setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_ATOP);
                            }
                        });

                        Log.d(TAG, "Перестаем показываеть ProgressBar");
                        Log.d(TAG, "Успешно включили службу");
                    }
                }
            });
        }
        // выключаем службу
        else {
            Request request = new Request.Builder()
                    .url(setOffUrl)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    // если выключить не удалось, возвращаем кнопку в состояние "вкл"
                    currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buttonView.setOnCheckedChangeListener(null);
                            buttonView.setChecked(true);
                            buttonView.setOnCheckedChangeListener(StatusFragment.this::onCheckedChanged);
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            shieldImage.setVisibility(View.VISIBLE);
                            tvSystemStatus.setText(R.string.system_unknown);
                            shieldImage.setColorFilter(getResources().getColor(R.color.colorYellow), PorterDuff.Mode.SRC_ATOP);
                            Toast.makeText(currentActivity.getApplicationContext(), "Выключение не удалось", Toast.LENGTH_SHORT).show();
                        }
                    });

                    Log.d(TAG, "Перестаем показываеть ProgressBar");
                    Log.d(TAG, "Выключение службы не удалось");
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try (ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful())
                            throw new IOException("Unexpected code " + response);
                        // При успешном запросе меняем состояние поля на "выкл"
                        currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvSystemStatus.setText(R.string.system_inactive);
                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                                shieldImage.setVisibility(View.VISIBLE);
                                shieldImage.setColorFilter(getResources().getColor(R.color.colorGrey), PorterDuff.Mode.SRC_ATOP);
                            }
                        });

                        Log.d(TAG, "Перестаем показываеть ProgressBar");
                        Log.d(TAG, "Успешно выключили службу");
                    }
                }
            });
        }
    }
}
