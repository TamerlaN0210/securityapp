package com.example.securityapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.util.Log;
import android.view.MenuItem;

import Helpers.FirebaseCloudUserSaverHelper;


public class MainActivity extends AppCompatActivity implements
        StatusFragment.OnFragmentInteractionListener,
        CameraFragment.OnFragmentInteractionListener,
        AlertFragment.OnFragmentInteractionListener {

    final String TAG = MainActivity.class.getSimpleName();
    BottomNavigationView bottomMenu;
    String field_image_url, field_title, field_desc;
    static Boolean isAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        field_image_url = Conf.dpImageUrl;
        field_title = Conf.dpTitle;
        field_desc = Conf.dpDescription;
        bottomMenu = (BottomNavigationView) findViewById(R.id.bottomNavMenu);
        // открываем фрагмент с сообщениями, когда тапаем на нотификацию
        if (!getIntent().hasExtra(field_image_url)) {
            // При создании активити инициализируем стартовый фрагмент "StatusFragment"
            getSupportFragmentManager().beginTransaction().replace(R.id.mainFrameLayout, new StatusFragment()).commit();
        } else {
            Log.d(TAG, "Получили ненулевое начальное состояние");
            isAlarm = true;
            Bundle bundle = new Bundle();
            bundle.putString(field_image_url, getIntent().getStringExtra(field_image_url));
            if (getIntent().hasExtra(field_title)) {
                bundle.putString(field_title, getIntent().getStringExtra(field_title));
            }
            if (getIntent().hasExtra(field_desc)) {
                bundle.putString(field_desc, getIntent().getStringExtra(field_desc));
            }
            AlertFragment alertFragment = new AlertFragment();
            alertFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.mainFrameLayout, alertFragment).commit();
            bottomMenu.setSelectedItemId(R.id.action_alerts);
        }

        // Нижнее меню
        bottomMenu.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        switch (menuItem.getItemId()) {
                            case R.id.action_status:
                                StatusFragment statusFragment = new StatusFragment();
                                if ( isAlarm != null) {
                                    Bundle statusBundle = new Bundle();
                                    statusBundle.putBoolean("isAlarm", isAlarm);
                                }
                                transaction.replace(R.id.mainFrameLayout, statusFragment).commit();
                                return true;
                            case R.id.action_cameras:
                                transaction.replace(R.id.mainFrameLayout, new CameraFragment()).commit();
                                return true;
                            case R.id.action_alerts:
                                transaction.replace(R.id.mainFrameLayout, new AlertFragment()).commit();
                                return true;
                            default:
                                return false;
                        }
                    }
                }
        );

        // Сохраняем актуальный дата-пуш токен при каждом запуске приложения
        new FirebaseCloudUserSaverHelper().saveUser(getApplicationContext());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("qwe", "New Intent");
        field_image_url = Conf.dpImageUrl;
        field_title = Conf.dpTitle;
        field_desc = Conf.dpDescription;
        if (!intent.hasExtra(field_image_url)) {
            // При создании активити инициализируем стартовый фрагмент "StatusFragment"
            getSupportFragmentManager().beginTransaction().replace(R.id.mainFrameLayout, new StatusFragment()).commit();
        } else {
            Log.d(TAG, "Получили ненулевое начальное состояние в onNewIntent");
            Bundle bundle = new Bundle();
            bundle.putString(field_image_url, intent.getStringExtra(field_image_url));
            if (intent.hasExtra(field_title)) {
                bundle.putString(field_title, intent.getStringExtra(field_title));
            }
            if (intent.hasExtra(field_desc)) {
                bundle.putString(field_desc, intent.getStringExtra(field_desc));
            }
            AlertFragment alertFragment = new AlertFragment();
            alertFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.mainFrameLayout, alertFragment).commit();
            bottomMenu.setSelectedItemId(R.id.action_alerts);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void onBackPressed() {
        // Для правильной работы меню, переопределяем кнопку назад, чтобы приложение всегда сворачивалось
        moveTaskToBack(true); // exist app
    }

    public boolean checkInternetConection() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
