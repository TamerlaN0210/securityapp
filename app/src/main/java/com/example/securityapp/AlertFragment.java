package com.example.securityapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import Helpers.OkhttpImageDownloader;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AlertFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AlertFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AlertFragment extends Fragment {
    final String TAG = AlertFragment.class.getSimpleName();
    TextView tv_title, tv_description;
    ImageView image_alert;
    Bitmap notification_bitmap;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AlertFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AlertFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AlertFragment newInstance(String param1, String param2) {
        AlertFragment fragment = new AlertFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_alert, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // названия полей переменных
        String field_image_url = Conf.dpImageUrl;
        String field_title = Conf.dpTitle;
        String field_desc = Conf.dpDescription;
        tv_title = view.findViewById(R.id.textAlertTitle);
        tv_description = view.findViewById(R.id.textAlertDescription);
        image_alert = view.findViewById(R.id.imageAlert);
        setRetainInstance(true);

        if (this.getArguments() != null) {
            String image_url = this.getArguments().getString(field_image_url);
            Log.d(TAG, "Input parameters: " + this.getArguments().toString());
            // проверяем наличие URL картинки
            if (image_url != null) {
                OkhttpImageDownloader okHttpHandler = new OkhttpImageDownloader();
                try {
                    notification_bitmap = okHttpHandler.execute(image_url).get();
                    image_alert.setImageBitmap(notification_bitmap);
                    image_alert.setVisibility(View.VISIBLE);
                    Log.d("qwe", "картинка есть");
                } catch (Exception e) {
                    Log.d(TAG, "Something wrong ", e);
                }
            }
            // проверяем наличие заголовка
            if (this.getArguments().getString(field_title) != null) {
                tv_title.setText(this.getArguments().getString(field_title));
                tv_title.setVisibility(View.VISIBLE);
                Log.d("qwe", "Тайтл есть");
            }
            // проверяем наличие описания
            if (this.getArguments().getString(field_desc) != null) {
                tv_description.setText(this.getArguments().getString(field_desc));
                tv_description.setVisibility(View.VISIBLE);
                Log.d("qwe", "Описание есть");
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
